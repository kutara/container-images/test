#!/usr/bin/env bash
set -x

ctr1=$(buildah from --arch "$ARCH" registry.fedoraproject.org/fedora)

## Get all updates and install our minimal httpd server!
#buildah run "$ctr1" -- dnf update -y
#buildah run "$ctr1" -- dnf install -y lighttpd
#buildah run "$ctr1" -- mkdir /run/lighttpd
buildah run "$ctr1" -- echo hi!

## Include some buildtime annotationssa
buildah config --annotation "com.example.build.host=$(uname -n)" "$ctr1"

## Run our server and expose the port
buildah config --cmd "/usr/sbin/lighttpd -D -f /etc/lighttpd/lighttpd.conf" "$ctr1"
buildah config --port 80 "$ctr1"
